var express = require("express");
var router = express.Router();
var Controller = require("../apiv1/home");
var Jwtmethods = require("../shared/jwt");
const { body } = require("express-validator/check");

router.post(
    "/event",
    [
        body("title").exists(),
        body("draftIcon").exists(),
        body("date").exists(),
        body("state").exists(),
        body("type").exists(),
        body("location").exists(),
        body("image").exists()
    ],
    Controller.addEvent
);

router.get("/event", Controller.getEventList);

module.exports = router;
