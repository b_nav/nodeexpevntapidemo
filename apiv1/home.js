const methods = {},
    Event = require("../models/event"),
    { check, validationResult, body } = require("express-validator/check"),
    config = require("../config"),
    mongoose = require("mongoose"),
    ba64 = require("ba64");
//  add event

methods.addEvent = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res
            .status(config.errorCodes.validation)
            .json({ errors: errors.array() });
    }
    let payload = req.body;

    let filename = Date.now().toString();
    let fileext = ba64.getExt(payload.image);

    ba64.writeImage("../demoapp/public/" + filename, payload.image, function(
        err
    ) {
        if (err) throw err;

        console.log("Image saved successfully");
    });

    var newEvent = new Event({
        title: payload.title,
        draftIcon: payload.draftIcon,
        date: payload.date,
        state: payload.state,
        type: payload.type,
        location: payload.location,
        image: filename + "." + fileext
    });

    newEvent.save(function(err) {
        if (err) {
            return res.status(200).json({
                status: false,
                msg: "Error in saving data. Please try again",
                err
            });
        } else {
            return res
                .status(200)
                .json({ status: true, msg: "data added succesfully." });
        }
    });
};

methods.getEventList = (req, res, next) => {
    Event.find({}, function(err, events) {
        if (err) {
            return res.send({ status: false, msg: "Error", err });
        } else {
            res.send({ status: true, events });
        }
    });
};

module.exports = methods;
