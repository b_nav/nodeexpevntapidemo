const mongoose = require("mongoose");
var event_schema = mongoose.Schema({
    title: {
        type: String,
        required: false
    },
    draftIcon: {
        type: String,
        required: false
    },
    date: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: false
    },
    location: {
        type: String,
        required: false
    },
    image: {
        type: String,
        required: false
    }
});

var schema = (module.exports = mongoose.model("event", event_schema));
