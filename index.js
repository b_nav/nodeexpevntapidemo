var express = require("express");
var app = express();

var fs = require("fs");
var bodyParser = require("body-parser");

/*CORS */
var allowCrossDomain = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "*");
    next();
};
var cors = require("cors");
app.use(cors());

/*  MONGO DB CONNECT*/
const mongoose = require("mongoose");
// connect to mongodb
mongoose.connect("mongodb://localhost/demo_app", { useNewUrlParser: true });
const db = mongoose.connection;
mongoose.set("useFindAndModify", false);
//check connection
db.once("open", function() {
    console.log("connected to mongodb");
});

//check for database errors
db.on("error", function(err) {
    console.log(err);
});

/*TO PARSE JSON DATA*/
app.use(bodyParser.json());
//To parse URL encoded data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(allowCrossDomain);

/*STATIC DIRECORATY PATHS*/
app.use(express.static("public"));

/*------------------------------V1 Apis routing----------------------------------------------------*/

var Router = require("./routes/home.js");
app.use("/api/v1", Router);

app.listen(3001, () => console.log("http server running on 3001"));
